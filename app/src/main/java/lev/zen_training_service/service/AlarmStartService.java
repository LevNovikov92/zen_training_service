package lev.zen_training_service.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import lev.zen_training_service.reciever.Alarm;

public class AlarmStartService extends Service {

    Alarm alarm = new Alarm();
    public AlarmStartService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        alarm.CancelAlarm(this);
        Log.i("ALARM", "Destroy service");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        alarm.SetAlarm(this);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

package lev.zen_training_service.service;

import android.app.IntentService;
import android.content.Intent;

import java.util.concurrent.TimeUnit;

public class TimeIntentService extends IntentService {
    public static final String ACTION_GET_MESSAGE = "lev.zen_training_service.action.MESSAGE";
    public static final String ACTION_SET_PROGRESS = "lev.zen_training_service.action.PROGRESS";
    public static final String ACTION_SET_RESPONSE = "lev.zen_training_service.action.RESPONSE";

    // TODO: Rename parameters
    public static final String EXTRA_DURATION = "lev.zen_training_service.extra.DURATION";
    public static final String EXTRA_MESSAGE = "lev.zen_training_service.extra.MESSAGE";
    public static final String EXTRA_PROGRESS = "lev.zen_training_service.extra.PROGRESS";

    public TimeIntentService() {
        super("TimeIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        System.out.println("onHandle");
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_MESSAGE.equals(action)) {
                final int param1 = intent.getIntExtra(EXTRA_DURATION, 0);
                final String param2 = intent.getStringExtra(EXTRA_MESSAGE);
                handleActionGetMessage(param1, param2);
            }
        }
    }

    private void handleActionGetMessage(int duration, String message) {
        for(int i=1; i<=duration; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int progress = i*100/duration;
            sendProgress(progress);
        }
        System.out.println("Test OK/ Message: " + message);
        sendMessage(message);
    }

    private void sendMessage(String message) {
        Intent intent = new Intent();
        intent.setAction(ACTION_SET_RESPONSE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra(EXTRA_MESSAGE, message);
        sendBroadcast(intent);
    }

    private void sendProgress(int progress) {
        Intent intent = new Intent();
        intent.setAction(ACTION_SET_PROGRESS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra(EXTRA_PROGRESS, progress);
        sendBroadcast(intent);
    }
}

package lev.zen_training_service.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import lev.zen_training_service.reciever.TestWakefulReceiver;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class WakefulService extends IntentService {

    public static final String ACTION_WAKEFUL = "lev.zen_training_service.WAKEFUL";

    public WakefulService() {
        super("WakefulService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
        final String action = intent.getAction();
            for(int i=0; i<3; i++) {
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.i("WAKEFUL", "Complete Wakeful service");
            TestWakefulReceiver.completeWakefulIntent(intent);
        }
    }
}

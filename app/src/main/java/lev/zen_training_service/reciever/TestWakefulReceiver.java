package lev.zen_training_service.reciever;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import lev.zen_training_service.service.WakefulService;

/**
 * Created by Lev on 05.01.2016.
 */
public class TestWakefulReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("WAKEFUL", "Service started!");
        Intent serviceIntent = new Intent(context, WakefulService.class);
        startWakefulService(context, serviceIntent);
    }
}

package lev.zen_training_service.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import lev.zen_training_service.R;
import lev.zen_training_service.service.TimeIntentService;


public class IntentServiceActivity extends Activity {

    private MessageBroadcastReceiver mMessageReceiver;
    private ProgressBroadcastReceiver mProgressReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_service);

        Button startServiceButton = (Button) findViewById(R.id.launchButton);
        startServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TimeIntentService.class);
                intent.putExtra(TimeIntentService.EXTRA_DURATION, 7);
                intent.putExtra(TimeIntentService.EXTRA_MESSAGE, "Test OK");
                intent.setAction(TimeIntentService.ACTION_GET_MESSAGE);
                startService(intent);
            }
        });


        mMessageReceiver = new MessageBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(
                TimeIntentService.ACTION_SET_RESPONSE);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(mMessageReceiver, intentFilter);

        mProgressReceiver = new ProgressBroadcastReceiver();
        intentFilter = new IntentFilter(TimeIntentService.ACTION_SET_PROGRESS);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(mProgressReceiver, intentFilter);
    }

    public class MessageBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra(TimeIntentService.EXTRA_MESSAGE);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public class ProgressBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int progress = intent.getIntExtra(TimeIntentService.EXTRA_PROGRESS, 0);
            System.out.println(progress);
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setProgress(progress);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
        unregisterReceiver(mProgressReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_intent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package lev.zen_training_service.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import lev.zen_training_service.R;
import lev.zen_training_service.service.AlarmStartService;


public class StartActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Button intentServiceBtn = (Button) findViewById(R.id.startIntentServiceTestButton);
        intentServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), IntentServiceActivity.class);
                startActivity(intent);
            }
        });

        Button wakefulServiceBtn = (Button) findViewById(R.id.startWakefulBR);
        wakefulServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WakefulBRActivity.class);
                startActivity(intent);
            }
        });

        Button alarmStartServiceBtn = (Button) findViewById(R.id.startAlarmServiceButton);
        alarmStartServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AlarmStartService.class);
                startService(intent);
                Log.i("ALARM", "Service started");
            }
        });

        Button alarmStopServiceBtn = (Button) findViewById(R.id.stopAlarmServiceButton);
        alarmStopServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AlarmStartService.class);
                stopService(intent);
                Log.i("ALARM", "Service has been stopped");
            }
        });

        Button startSmsReaderBtn = (Button) findViewById(R.id.startSmsReader);
        startSmsReaderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SmsReaderActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package lev.zen_training_service.activity;

import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import lev.zen_training_service.R;

public class SmsReaderActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_reader);

        Button readSmsButton = (Button) findViewById(R.id.readSmsButton);
        readSmsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SmsReadTask().execute();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sms_reader, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SmsReadTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
            String data = "";
            if (cursor.moveToFirst()) {
                int i = 0;
                do {
                    i++;
                    String msgData = "";
                    try {
                        msgData = cursor.getString(cursor.getColumnIndexOrThrow("address")) + "\n" +
                                cursor.getString(cursor.getColumnIndexOrThrow("body")) + "\n\n";
                    } catch (IllegalArgumentException e) {
                        Log.e("SmsReader", e.toString());
                    }
                    data+= msgData;
                } while (cursor.moveToNext() && i<5);
            } else {
                data = "Sms not found";
            }
            return data;
        }

        @Override
        protected void onPostExecute(String data) {
            super.onPostExecute(data);
            TextView textView = (TextView) findViewById(R.id.smsTextView);
            textView.setText(data);
        }
    }
}
